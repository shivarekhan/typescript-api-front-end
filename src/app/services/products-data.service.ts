import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class ProductsDataService {
  API_ENDPOINT = 'https://demo-restapi.onrender.com/getProducts';
  API_ENDPOINT_ADD_PRODUCTS = 'https://demo-restapi.onrender.com/addProduct';
  API_ENDPOINT_DELETE = 'https://demo-restapi.onrender.com/deleteProduct';
  API_ENDPOINT_UPDATE = 'https://demo-restapi.onrender.com/updateProduct';
  constructor(private http: HttpClient) {}
  products() {
    return this.http.get(this.API_ENDPOINT);
  }
  saveProduct(data: any) {
    return this.http.post(this.API_ENDPOINT_ADD_PRODUCTS, data);
  }
  deleteEntry(id: number) {
    return this.http.delete(`${this.API_ENDPOINT_DELETE}/${id}`);
  }
  updateEntry(data: any) {
    return this.http.post(this.API_ENDPOINT_UPDATE,data);
  }
}
