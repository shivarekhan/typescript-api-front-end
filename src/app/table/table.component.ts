import { Component } from '@angular/core';
import { ProductsDataService } from '../services/products-data.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
})
export class TableComponent {
  products: any = [];
  isShow: boolean = false;

  constructor(private productsData: ProductsDataService) {
    productsData.products().subscribe((data) => {
      console.log(data);
      this.products = data;
    });
  }

  deleteEntry(id: number) {
    console.warn(this.products);
    this.productsData.deleteEntry(id).subscribe((data) => {
      console.warn(data);
      this.products = this.products.filter((current: any) => {
        if (current.id != id) {
          return current;
        }
      });
    });
  }

  updateShow() {
    // this.isShow = !this.isShow;
  }

  updateEntry(data: any) {
    this.productsData.updateEntry(data).subscribe((data) => {
      console.warn(data);
    });
  }
  successMsg: boolean = false;

  restModal() {
    this.successMsg = false;
  }

  getUserFormData(data: any) {
    this.successMsg = false;
    console.warn(data);
    this.productsData.saveProduct(data).subscribe((result) => {
      console.warn(result);
      this.products.push(data);
      this.successMsg = true;
    });
  }
}
